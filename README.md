# Jogo de azar

Central de jogos de azar (em construção)

Site criado para a entrega do final da Sprint 3 na Kenzie Academy Brasil.

Teremos 4 jogos disponíveis: 

- Máquina Caça-Níquel (já disponivel)
- Pedra, papel e tesoura. (em breve)
- Magic 8-Ball (em breve)
- Caça-palavras (em breve)


Bora se divertir e testar a sorte hoje :)